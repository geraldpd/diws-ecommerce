# Diws - Ecommerce

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://gitlab.com/geraldpd/diws-ecommerce/-/tree/master)

Diws - Ecommerce

  - Fetch products in ebay by keyword or category
  - Update your shopify store

## Installation

```sh
$ composer require diws/ecommerce
```
In your **config/app.php**, under *package providers* put

 > Diws\Ecommerce\EcommerceServiceProvider::class

  Run the following command

```sh
$ php artisan vendor:publish --provider="Diws\Ecommerce\EcommerceServiceProvider"
```
  ecommerce.php file should then be available in your config folder, containing the values below.


     'amazon' => [], //comming soon

    'ebay' => [
        'base_url' => 'https://svcs.ebay.com/services/search/FindingService/v1',
        'tracking_id' => 0,
        'network_id' => 0
    ],

    'shopify' => [
        'api_key' => '',
        'password' => '',
        'shared_secret' => '',
        'my_shopify_store' => '',
        'api_version' => '2020-07',
    ],

 Fetch your Ebay and shopify credentials in your account.

 ## Usage

 ### Ebay
 > use Diws\Ecommerce\Ebay\Ebay;

Here's how you can use the Ebay module.

`NOTE: search_method is always required.`

#### Search product by Category

`category_id` comes from Ebays database.

```sh
public function foo()
{
$ebay = new Ebay;

  return $ebay->searchProducts([
        'search_method' => 'category',
        'category_id' => '9355',
  ]);
}
```

![alt text](./docs/ss5.png)

#### Search product by Keyword
```sh
public function foo()
{
$ebay = new Ebay;

  return $ebay->searchProducts([
        'search_method' => 'keyword',
        'keywords' => 'iphone 12',
  ]);
}
```

You should then have a simillar result.
![alt text](./docs/ss1.png)


#### Search product by Store
```sh
public function foo()
{
$ebay = new Ebay;

  return $ebay->searchProducts([
        'search_method' => 'store',
        'store_name' => 'myEbayStore',
        'keywords' => 'iphone 12', //optional
  ]);
}
```
Note: Store names are case sensitive. Also, if the store name contains an ampersand (&), you must use the & character entity (& amp;) in its place.

If you do not specify `store_name` in the request, the search is across all eBay stores. When searching across all stores, you must specify keywords.



#### Pagination
You can control the pagination by adding a **page** attribute in your paramater. by default
the page will always start in 1,

```sh
public function foo()
{
$ebay = new Ebay;

  return $ebay->searchProducts([
        'search_method' => 'keyword',
        'keywords' => 'iphone 12',
        'page' => '2',
  ]);
}
```

![alt text](./docs/ss2.png)

#### Entries per page
You can also customize the number of returned entries for each search made.
By default `entries_per_page` has a default value of 10. The maximum entries is 100.

```sh
public function foo()
{
$ebay = new Ebay;

  return $ebay->searchProducts([
        'search_method' => 'keyword',
        'keywords' => 'iphone 12',
        'page' => '2',
        'entries_per_page' => 20, //10 - 100
  ]);
}
```

![alt text](./docs/ss3.png)


#### Appending custom row per entry
You can append a custom row per entry by adding a `extra_columns` attribute in the parameters,

```sh
public function foo()
{
    $ebay = new Ebay;
    return $ebay->searchProducts([
        'search_method' => 'keyword',
        'keywords' => 'iphone 12',
        'extra_columns' => [
            'foo' => 'bar'
        ]
  ]);
}
```

![alt text](./docs/ss4.png)

#### Filtering result


[Read more about Ebay Item FIlter Options](https://developer.ebay.com/devzone/finding/callref/types/ItemFilterType.html)

Right now the Seller filter is only fully supported. youll need to append

```sh
public function foo()
{

  $ebay = new Ebay;

  return $ebay->searchProducts([
        'search_method' => 'keyword',
        'keywords' => 'iphone 12',
        'filters' => [
            [
                'name' => 'Seller',
                'value' => 'seller1',
                'value' => 'seller2'
            ]
        ]
  ]);
}
```

multiple values are allowed. Up to 100 sellers can be specified.
the seller_info and store_info attributes will be automatically included in reponse.

![alt text](./docs/ss6.png)
