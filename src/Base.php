<?php
namespace Diws\Ecommerce;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Base {

	public function processRequest($request_parameter, $raw = false)
	{
		//throw exception when no ecommerce config file is found
		if (!config('ecommerce')) {
			throw new \Exception('ecommerce.php config file not found.');
		}

		//determine if request is GET or POST
		$request_parameter['action'] = !isset($request_parameter['action']) ? 'GET' :$request_parameter['action'];
		//set the parameters
		$request_parameter['parameter'] = !isset($request_parameter['parameter']) ? [] :$request_parameter['parameter'];
		$client_options = [];

		//set the header if its provided
		if (isset($request_parameter['parameter']['headers'])) {
			$request_parameter['header'] = $request_parameter['parameter']['headers'];
			unset($request_parameter['parameter']['headers']);
			$client_options = [$request_parameter['header']];
		}

		//set the guzzle options
		$client = new Client($client_options);

		try
		{
			$response = $client->request(
				$request_parameter['action'],
				$request_parameter['url'],
				$request_parameter['parameter'],
			);
		}
		catch (ClientException $e)
		{
			if (!$e->hasResponse()) return 'error_with_no_response';

			$response = $e->getResponse();
			$fetched_response = [
				//'client' => $client, //!do not include in production.
				'parameters' => $request_parameter,
				'status_code' => $response->getStatusCode(),
				'reason' => $response->getReasonPhrase(),
				'body' => (string) $response->getBody()
			];
			return $fetched_response;
		}

		if ($raw) return $response->getBody()->getContents();

		$content_response = $response->getBody()->getContents();

		return collect(json_decode($content_response))->all();
	}

	public function buildEndpoint($endpoint = '', $query_parameter = null)
	{
		$query_parameter = isset($query_parameter['query_params']) ? '?'.http_build_query($query_parameter['query_params']) : '';

		return $this->api_startpoint.$endpoint.$query_parameter;
	}

}