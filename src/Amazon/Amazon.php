<?php
namespace Diws\Ecommerce\Amazon;

use Diws\Ecommerce\Base;

class Amazon extends Base
{

	private $aws_api_key;
	private $aws_api_secret_key;
	private $aws_associate_tag;

	public function __construct()
	{
		$this->aws_api_key = config('ecommerce.amazon.aws_api_key');
		$this->aws_api_secret_key = config('ecommerce.amazon.aws_api_secret_key');
		$this->aws_associate_tag = config('ecommerce.amazon.aws_associate_tag');

		/*
			curl "https://webservices.amazon.com/paapi5/searchitems" \
			-H "Host: webservices.amazon.com" \
			-H "Content-Type: application/json; charset=UTF-8" \
			-H "X-Amz-Date: 20210215T072403Z" \
			-H "X-Amz-Target: com.amazon.paapi5.v1.ProductAdvertisingAPIv1.SearchItems" \
			-H "Content-Encoding: amz-1.0" \
			-H "User-Agent: paapi-docs-curl/1.0.0" \
			-H "Authorization: AWS4-HMAC-SHA256 Credential=AKIAIPVZCDVNLC7AZWKQ/20210215/us-east-1/ProductAdvertisingAPI/aws4_request SignedHeaders=content-encoding;host;x-amz-date;x-amz-target Signature=6451e7f0bb90895783650b6c3481e8cf614a75aa5e363854b8fdc5e570ccb8f6" \
			-d "{\"Marketplace\":\"www.amazon.com\",\"PartnerType\":\"Associates\",\"PartnerTag\":\"blackskin01-21\",\"Keywords\":\"kindle\",\"SearchIndex\":\"All\",\"ItemCount\":3,\"Resources\":[\"Images.Primary.Large\",\"ItemInfo.Title\",\"Offers.Listings.Price\"]}"
		*/

		$this->api_startpoint = 'https://webservices.amazon.com/paapi5/searchitems';
	}

	public function searchProducts()
	{
		$response = parent::processRequest([
			'action' => 'POST',
			'url' => parent::buildEndpoint(),
			'parameter' => [
			'headers' => [
				'Host' => 'webservices.amazon.com',
				'Content-Type' => 'application/json; charset=UTF-8',
				'X-Amz-Date' => '20210215T072403Z',
				'X-Amz-Target' => 'com.amazon.paapi5.v1.ProductAdvertisingAPIv1.SearchItems',
				'Content-Encoding' => 'amz-1.0',
				'User-Agent' => 'paapi-docs-curl/1.0.0',
				'Authorization' => 'AWS4-HMAC-SHA256 Credential=AKIAIPVZCDVNLC7AZWKQ/20210215/us-east-1/ProductAdvertisingAPI/aws4_request SignedHeaders=content-encoding;host;x-amz-date;x-amz-target Signature=6451e7f0bb90895783650b6c3481e8cf614a75aa5e363854b8fdc5e570ccb8f6'
			],
			'Marketplace' => 'www.amazon.com',
			'PartnerType' => 'Associates',
			'PartnerTag' => 'blackskin01-21',
			'Keywords' => 'kindle',
			'SearchIndex' => 'All',
			'ItemCoun' => '3',
			'Resources' => 'Images.Primary.Large'
			]
		]);

		return $response;
	}

}