<?php
namespace Diws\Ecommerce\Shopify;

trait ShopifyStore
{

	public function getShop()
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('shop.json')
		]);

		return $response;
	}

}