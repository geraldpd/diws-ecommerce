<?php
namespace Diws\Ecommerce\Shopify;

trait ShopifyOrder
{
	/*
		GET /admin/api/2020-07/orders.json?status=any
		Retrieves a list of orders
	*/
	public function getOrders($parameter = [])
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('orders.json', $parameter),
			'parameter' => $parameter
		]);

		return $response;
	}

	/*
		GET /admin/api/2020-07/orders/{order_id}.json
		Retrieves a specific order
	*/
	public function getOrder($order_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('orders/'. $order_id.'.json')
		]);

		return $response;
	}


	/*
		GET /admin/api/2020-07/orders/count.json
		Retrieves an order count
	*/
	public function getOrdersCount()
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('orders/count.json')
		]);

		return $response;
	}

	/*
		POST /admin/api/2020-07/orders/{order_id}/close.json
		Closes an order
	*/
	public function closeOrder($order_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('orders/'. $order_id .'/close.json'),
			'action' => 'POST'
		]);

		return $response;
	}

	/*
		POST /admin/api/2020-07/orders/{order_id}/open.json
		Re-opens a closed order
	*/
	public function openOrder($order_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('orders/'. $order_id .'/open.json'),
			'action' => 'POST'
		]);

		return $response;
	}

	/*
		POST /admin/api/2020-07/orders/{order_id}/cancel.json
		Cancels an order
		! Orders that have a fulfillment object can't be canceled.
		! For multi-currency orders, the currency property is required whenever the amount property is provided
	*/
	public function cancelOrder($order_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('orders/'. $order_id .'/cancel.json'),
			'action' => 'POST'
		]);

		return $response;
	}

	/*
		POST /admin/api/2020-07/orders.json
		Creates an order
	*/
	public function storeOrder($parameter)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('orders.json'),
			'action' => 'POST',
			'parameter' => $parameter
		]);

		return $response;
	}

	/*
		PUT /admin/api/2020-07/orders/{order_id}.json
		Updates an order
	*/
	public function updateOrder($order_id, $parameter)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('orders/'. $order_id.'.json'),
			'action' => 'PUT',
			'parameter' => $parameter
		]);

		return $response;
	}

	/*
		DELETE /admin/api/2020-07/orders/{order_id}.json
		Deletes an order
	*/
	public function deleteOrder($order_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('orders/'. $order_id.'.json'),
			'action' => 'DELETE'
		]);

		return $response;
	}
}