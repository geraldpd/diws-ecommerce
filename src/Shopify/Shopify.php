<?php
namespace Diws\Ecommerce\Shopify;

use Diws\Ecommerce\Base;
use Diws\Ecommerce\Shopify\ShopifyStore;
use Diws\Ecommerce\Shopify\ShopifyCollection;
use Diws\Ecommerce\Shopify\ShopifyProduct;
use Diws\Ecommerce\Shopify\ShopifyOrder;
use Diws\Ecommerce\Shopify\ShopifyDraftOrder;
use Diws\Ecommerce\Shopify\ShopifyCustomer;

class Shopify extends Base
{
  use ShopifyStore;
  use ShopifyCollection;
  use ShopifyProduct;
  use ShopifyOrder;
  use ShopifyDraftOrder;
  use ShopifyCustomer;

  private $access_token;
  private $api_key;
  private $password;
  private $shared_secret;
  private $my_shopify_store;

  protected $api_startpoint;

  public function __construct()
  {
    $this->access_token = '';
    $this->api_key = config('ecommerce.shopify.api_key');
    $this->password = config('ecommerce.shopify.password');
    $this->shared_secret = config('ecommerce.shopify.shared_secret');
    $this->my_shopify_store = config('ecommerce.shopify.my_shopify_store');
    $this->api_version = config('ecommerce.shopify.api_version');
    $this->api_startpoint = 'https://'. $this->api_key .':'. $this->password .'@'. $this->my_shopify_store .'/admin/api/'. $this->api_version . '/';
  }
}