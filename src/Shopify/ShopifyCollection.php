<?php
namespace Diws\Ecommerce\Shopify;

trait ShopifyCollection
{
	/*
		GET /admin/api/2020-07/collections/{collection_id}.json
		Retrieves a single collection
	*/
	public function getCollection($collection_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('collections/'. $collection_id.'.json')
		]);

		return $response;
	}

	/*
		GET /admin/api/2020-07/collections/{collection_id}/products.json
		Retrieve a list of products belonging to a collection
	*/
	public function getCollectionProducts($collection_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('collections/'. $collection_id.'/products.json')
		]);

		return $response;
	}

}