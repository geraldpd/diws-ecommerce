<?php
namespace Diws\Ecommerce\Shopify;

trait ShopifyCustomer
{

	/*
		GET /admin/api/2020-07/customers.json
		Retrieves a list of customers
	*/
	public function getCustomers($parameter = [])
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('customers.json', $parameter),
		]);

		return $response;
	}

	/*
		GET /admin/api/2020-07/customers/search.json?query=Bob country:United States
		Searches for customers that match a supplied query
		!https://shopify.dev/docs/admin-api/rest/reference/customers/customer#search-2020-07
	*/
	public function searchCustomers($parameter = [])
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('customers/search.json', $parameter),
			'parameter' => $parameter
		]);

		return $response;
	}

	/*
		GET /admin/api/2020-07/customers/{customer_id}.json
		Retrieves a single customer
	*/
	public function getCustomer($customer_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('customers/'.$customer_id.'.json'),
		]);

		return $response;
	}

	/*
		POST /admin/api/2020-07/customers.json
		Creates a customer
		!https://shopify.dev/docs/admin-api/rest/reference/customers/customer#create-2020-07
	*/
	public function storeCustomer($parameter = [])
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('customers.json', $parameter),
			'action' => 'POST',
			'parameter' => $parameter
		]);

		return $response;
	}

	/*
		PUT /admin/api/2020-07/customers/{customer_id}.json
		Updates a customer
	*/
	public function updateCustomer($customer_id, $parameter)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('customers/'.$customer_id.'.json'),
			'action' => 'PUT',
			'parameter' => $parameter
		]);

		return $response;
	}

	/*
		POST /admin/api/2020-07/customers/{customer_id}/account_activation_url.json
		Creates an account activation URL for a customer
	*/
	public function getCustomerAccountActivationUrl($customer_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('customers/'.$customer_id.'/account_activation_url.json'),
			'action' => 'POST',
		]);

		return $response;
	}

	/*
		POST /admin/api/2020-07/customers/{customer_id}/send_invite.json
		Sends an account invite to a customer
	*/
	public function sendCustomerInvite($customer_id, $parameter = [])
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('customers/'.$customer_id.'/send_invite.json'),
			'action' => 'POST',
			'parameter' => $parameter,
		]);

		return $response;
	}

	/*
		DELETE /admin/api/2020-07/customers/{customer_id}.json
		Deletes a customer.
	*/
	public function deleteCustomer($customer_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('customers/'.$customer_id.'.json'),
			'action' => 'DELETE',
		]);

		return $response;
	}

	/*
		GET /admin/api/2020-07/customers/count.json
		Retrieves a count of customers
	*/
	public function getCustomersCount()
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('customers/count.json'),
		]);

		return $response;
	}

	/*
		GET /admin/api/2020-07/customers/{customer_id}/orders.json
		Retrieves all orders belonging to a customers
	*/
	public function getCustomerOrders($customer_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('customers/'.$customer_id.'/orders.json'),
		]);

		return $response;
	}
}