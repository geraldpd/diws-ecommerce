<?php
namespace Diws\Ecommerce\Shopify;

trait ShopifyProduct
{
	/*
		GET /admin/api/2020-07/products.json
		Retrieves a list of products
	*/
	public function getProducts($parameter = [])
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('products.json', $parameter),
			'parameter' => $parameter
		]);

		return $response;
	}

	/*
		GET /admin/api/2020-07/products/count.json
		Retrieves a count of products
	*/
	public function getProductsCount()
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('products/count.json')
		]);

		return $response;
	}

	/*
		GET /admin/api/2020-07/products/{product_id}.json
		Retrieves a single product
		sample
			product_id : 5779189334178
	*/
	public function getProduct($product_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('products/'.$product_id.'.json')
		]);

		return $response;
	}

	/*
		POST /admin/api/2020-07/products.json
		Creates a new product
	*/
	public function storeProduct($parameter)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('products.json'),
			'action' => 'POST',
			'parameter' => $parameter
		]);

		return $response;
	}

	/*
		PUT /admin/api/2020-07/products/{product_id}.json
		Updates a product
		documentation : https://shopify.dev/docs/admin-api/rest/reference/products/product#update-2020-07
	*/
	public function updateProduct($product_id, $parameter = [])
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('products/'.$product_id.'.json'),
			'action' => 'PUT',
			'parameter' => $parameter
		]);

		return $response;
	}

	/*
		DELETE /admin/api/2020-07/products/{product_id}.json
		Deletes a product
		documentation : https://shopify.dev/docs/admin-api/rest/reference/products/product#destroy-2020-07
	*/
	public function deleteProduct($product_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('products/'.$product_id.'.json'),
			'action' => 'DELETE'
		]);

		return $response;
	}
}