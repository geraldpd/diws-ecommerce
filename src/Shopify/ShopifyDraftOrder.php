<?php
namespace Diws\Ecommerce\Shopify;

trait ShopifyDraftOrder
{
	/*
		POST /admin/api/2020-07/draft_orders.json
		Create a new DraftOrder
		! https://shopify.dev/docs/admin-api/rest/reference/orders/draftorder#create-2020-07
	*/
	public function storeDraftOrder($parameter)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('draft_orders.json'),
			'action' => 'POST',
			'parameter' => $parameter
		]);

		return $response;
	}

	/*
		PUT /admin/api/2020-07/draft_orders/{draft_order_id}.json
		Modify an existing DraftOrder
	*/
	public function updateDraftOrder($draft_order_id, $parameter = [])
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('draft_orders/'.$draft_order_id.'.json'),
			'action' => 'PUT',
			'parameter' => $parameter
		]);

		return $response;
	}

	/*
		GET /admin/api/2020-07/draft_orders.json
		Retrieves a list of draft orders
	*/
	public function getDraftOrders($parameter = [])
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('draft_orders.json', $parameter),
		]);

		return $response;
	}

	/*
	GET /admin/api/2020-07/draft_orders/{draft_order_id}.json
	Receive a single DraftOrder
	*/
	public function getDraftOrder($draft_order_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('draft_orders/'.$draft_order_id.'.json'),
		]);

		return $response;
	}

	/*
		GET /admin/api/2020-07/draft_orders/count.json
		Receive a count of all DraftOrders
	*/
	public function getDraftOrdersCount()
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('draft_orders/count.json'),
		]);

		return $response;
	}

	/*
	POST /admin/api/2020-07/draft_orders/{draft_order_id}/send_invoice.json
	Send an invoice
	*/
	public function sendDraftOrderInvoice($draft_order_id, $parameter = [])
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('draft_orders/'.$draft_order_id.'/send_invoice.json'),
			'action' => 'POST',
			'parameter' => $parameter
		]);

		return $response;
	}

	/*
	DELETE /admin/api/2020-07/draft_orders/{draft_order_id}.json
	Remove an existing DraftOrder
	*/
	public function deleteDraftOrder($draft_order_id)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('draft_orders/'.$draft_order_id.'.json'),
			'action' => 'DELETE',
		]);

		return $response;
	}

	/*
	PUT /admin/api/2020-07/draft_orders/{draft_order_id}/complete.json
	Complete a draft order
	*/
	public function completeDraftOrder($draft_order_id, $parameter)
	{
		$response = parent::processRequest([
			'url' => parent::buildEndpoint('draft_orders/'.$draft_order_id.'/complete.json', $parameter),
			'action' => 'PUT',
		]);

		return $response;
	}
}