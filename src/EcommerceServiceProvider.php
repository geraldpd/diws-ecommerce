<?php
namespace Diws\Ecommerce;

use Illuminate\Support\ServiceProvider;

class EcommerceServiceProvider extends ServiceProvider
{
  public function register()
  {
    $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'ecommerce');
  }

  public function boot()
  {
    if ($this->app->runningInConsole()) {
        $this->publishes([
          __DIR__.'/../config/config.php' => config_path('ecommerce.php'),
        ], 'config');
    }
  }
}