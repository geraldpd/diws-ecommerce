<?php
namespace Diws\Ecommerce\Ebay;

use Diws\Ecommerce\Base;
use Illuminate\Support\Facades\Storage;

class Ebay extends Base
{
	private $ebay_base_url;
	private $tracking_id;
	private $network_id;

	Private $operation_name;
	Private $service_version;
	Private $security_appname;
	Private $global_id;
	Private $response_data_format;

	protected $api_startpoint;
	private $entries_per_page;

	private $parameters;

	Private $operations = [
		'keyword' => 'findItemsByKeywords', //search by keyword
		'category' => 'findItemsByCategory', //search by category id(category ids are provided by ebay)
		'store' => 'findItemsIneBayStores', //search by ebay store
	];

	protected $assumed_categories = []; //beta

	public function __construct()
	{
		$this->ebay_base_url = config('ecommerce.ebay.base_url');
		$this->tracking_id = config('ecommerce.ebay.tracking_id');
		$this->network_id = config('ecommerce.ebay.network_id');

		$this->operation_name = '';//'findItemsByKeywords';
		$this->service_version = '1.0.0';
		$this->security_appname = 'itsmylon-d76c-4351-8ac1-890b5f98af8d';
		$this->global_id = 'EBAY-GB';
		$this->response_data_format = 'JSON';
		$this->entries_per_page = 10;

		$this->api_startpoint = config('ecommerce.ebay.base_url');
	}

	/*
		Search products from ebay's findItemsByKeywords or findItemsByCategory API
		$parameters = [
			'search_method' => 'keyword', 							//required - options : 'keyword' or 'category'.
			'keywords' => 'Shoes'									//only required if search_method == 'keyword'
			'category_id' => '15709'								//only required if search_method == 'category'.
			'page' => 1, 											//optional - defaults to 1, paginate through pages.
            'entries_per_page' => 50,								//optional - defaults tp 10, number of entries per page^
            'extra_columns' => ['created_at' => Carbon::now()]		//optional - array of key value pair, to be appended to the returned array result.
        ]
	*/
	public function searchProducts($parameters)
	{
		if (!$parameters) return ['status' => 'error', 'message' => 'Search Parameters Required'];

		$this->parameters = $parameters;

		switch ($parameters['search_method']) {
			case 'store':
				$products = $this->byStore();
				break;

			case 'category':
				$products = $this->byCategory();
				break;

			default: //keyword
				$products = $this->byKeyword();
				$assumed_categories = $this->assumed_categories;
				return compact('parameters', 'products', 'assumed_categories');
				break;
		}

		return compact('parameters', 'products');
	}

	/*
		Retrieves Ebay lvl -1 category ids; for reference.
	*/
	public function getCategories()
	{
		$params = http_build_query([
			'callname' => 'GetCategoryInfo',
			'appid' => $this->security_appname,
			'version' => '729',
			'siteid' => 'ebay',
			'CategoryId' => -1,
			'IncludeSelector' => 'ChildCategories'
		]);

		$response = parent::processRequest(['url' => 'https://open.api.ebay.com/Shopping?'.$params], true);

		$xml = simplexml_load_string($response);
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);

		return $array;//['CategoryArray'];
	}

	/*
		filters the provided array of $products and returns two array ['new' => Array(), 'old' => Array()].
	*/
	public function filterProducts($products, $old_product_ids)
	{
		$filtered_products = [
			'new' => [],
			'old' => []
		];

		foreach($products as $index => $product)
		{
			if (in_array($product['source_product_id'], $old_product_ids)) {
				$filtered_products['old'][$index] = $product;
			} else {
				$filtered_products['new'][$index] = $product;
			}
		}

		return $filtered_products;
	}

	private function byStore()
	{
		$request_parameter = $this->parameterBuilder();
		$request_parameter['storeName'] = $this->parameters['store_name'];
		$request_parameter['keywords'] = $this->parameters['keywords'];

		$response = parent::processRequest([
			'url' => parent::buildEndpoint('' , ['query_params' => $request_parameter]),
		]);

		$this->operation_name = 'findItemsIneBayStores';

		return $this->processResponse($response);
	}

	private function byKeyword()
	{
		$request_parameter = $this->parameterBuilder();
		$request_parameter['keywords'] = $this->parameters['keywords'];

		$response = parent::processRequest([
			'url' => parent::buildEndpoint('' , ['query_params' => $request_parameter]),
		]);

		$this->operation_name = 'findItemsByKeywords';

		return $this->processResponse($response);
	}

	private function byCategory()
	{
		$request_parameter = $this->parameterBuilder();
		$request_parameter['categoryId'] = $this->parameters['category_id'];

		$response = parent::processRequest([
			'url' => parent::buildEndpoint('' , ['query_params' => $request_parameter]),
		]);

		$this->operation_name = 'findItemsByCategory';
		return $this->processResponse($response);
	}

	function parameterBuilder()
	{
		$parameter = $this->parameters;
		$ebay_parameters = [
			'OPERATION-NAME' => $this->operations[$parameter['search_method']],
			'SERVICE-VERSION' => $this->service_version,
			'SECURITY-APPNAME' => $this->security_appname,
			'GLOBAL-ID' => $this->global_id,
			'RESPONSE-DATA-FORMAT' => $this->response_data_format,
			'REST-PAYLOAD' => '',
			'affiliate.trackingId' => $this->tracking_id,
			'affiliate.networkId' => $this->network_id,
			'paginationInput.entriesPerPage' => isset($parameter['entries_per_page']) ? $parameter['entries_per_page'] : $this->entries_per_page,
			'paginationInput.pageNumber' => isset($parameter['page']) ? $parameter['page'] : 1
		];

		if (isset($parameter['filters'])) {
			foreach ($parameter['filters'] as $filter_index => $filter) {
				$ebay_parameters["itemFilter({$filter_index}).name"] = $filter['name'];

				$filter_value = '';
				foreach ($filter['value'] as $value_index => $value) {
					$ebay_parameters["itemFilter({$filter_index}).value({$value_index})"] = $value;
				}
			}
			$ebay_parameters['outputSelector(0)'] = 'SellerInfo';
			$ebay_parameters['outputSelector(1)'] = 'StoreInfo';
		}

		return $ebay_parameters;
	}

	function processResponse($response)
	{
		$ebay_items = [];
		$this->assumed_categories = [];

		foreach($response[$this->operation_name.'Response'] as $response_key => $response_item)
		{
			foreach($response_item->searchResult as $result)
			{
				if(!isset($result->item)) return $ebay_items;

		        foreach($result->item as $result_key => $item)
		        {
		        	$ebay_item = json_decode(json_encode($item), true);

		        	$category_name = $ebay_item['primaryCategory'][0]['categoryName'][0];
		        	$category_id = $ebay_item['primaryCategory'][0]['categoryId'][0];

		        	if(!array_key_exists($category_id, $this->assumed_categories))
		        	{
		        		$this->assumed_categories[$category_id] = $category_name;
		        	}

					$item = [
		                'source_product_id' => $ebay_item['itemId'][0],
		                'source_category_name' => $category_name,
		                'source_category_id' => $category_id,
		                'title' => $this->removeEmoji($ebay_item['title'][0]),
		                'affiliate_url' => $ebay_item['viewItemURL'][0],
		                'image' => json_encode([$ebay_item['galleryURL'][0]]),
		                'price' => $ebay_item['sellingStatus'][0]['currentPrice'][0]['__value__'],
		                'currency' => $ebay_item['sellingStatus'][0]['currentPrice'][0]['@currencyId'],
		                'site' =>'ebay',
		                'site_url' => 'http://www.ebay.co.uk',
					];

					if (isset($this->parameters['extra_columns'])) {
						foreach($this->parameters['extra_columns'] as $column => $value){
							$item[$column] = $value;
						};
					}

					if (isset($this->parameters['filters'])) {
					//if (isset($this->parameters['filters']) || $this->parameters['search_method'] == 'store') {
						$seller_info = $ebay_item['sellerInfo'][0];
						$store_info = $ebay_item['storeInfo'][0];

						$item['seller_info'] = json_encode([
							'seller_user_name' => $seller_info['sellerUserName'][0],
							'feedback_score' => $seller_info['feedbackScore'][0],
							'positive_feedback_percent' => $seller_info['positiveFeedbackPercent'][0],
							'feedback_rating_star' => $seller_info['feedbackRatingStar'][0],
							'top_rated_seller' => $seller_info['topRatedSeller'][0]
						]);

						$item['store_info'] = json_encode([
							'store_name' => $store_info['storeName'][0],
							'store_url' => $store_info['storeURL'][0]
						]);
					}

		        	array_push($ebay_items, $item);
		        }
		    }
		}

		$this->storeCategory($this->assumed_categories); //store the keywords based on the returned items
		return $ebay_items;
	}

	function storeCategory($new_categories)
	{
		$categories = $new_categories;
		$exists = Storage::disk('local')->exists('public/assumed_categories.json');

		if($exists){
			$raw_categories = Storage::get('public/assumed_categories.json');
			$categories = (array)json_decode($raw_categories);

			foreach ($new_categories as $new_category_id => $new_category_name) {
		   		if(!array_key_exists($new_category_id, $categories))
		   		{
		   			$categories[$new_category_id] = $new_category_name;
		   		}
			}
		}

		Storage::disk('local')->put('public/assumed_categories.json', json_encode($categories));
	}

	function removeEmoji($string) {

		if (!config('unicode')) return $string;

		// Match Emoticons
		$regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
		$clear_string = preg_replace($regex_emoticons, '', $string);

		$regex_emoticons = '/[\x{1F900}-\x{1F999}]/u';
		$clear_string = preg_replace($regex_emoticons, '', $string);

		// Match Miscellaneous Symbols and Pictographs
		$regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
		$clear_string = preg_replace($regex_symbols, '', $clear_string);

		// Match Transport And Map Symbols
		$regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
		$clear_string = preg_replace($regex_transport, '', $clear_string);

		// Match Miscellaneous Symbols
		$regex_misc = '/[\x{2600}-\x{26FF}]/u';
		$clear_string = preg_replace($regex_misc, '', $clear_string);

		// Match Dingbats
		$regex_dingbats = '/[\x{2700}-\x{27BF}]/u';
		$clear_string = preg_replace($regex_dingbats, '', $clear_string);

        $unicode = config('unicode.unicode');
		$regex_code = '';

		foreach ($unicode as $code)
		{
			$regex_code .= "\x{{$code}}";
		}
		$clear_string = preg_replace('/['.$regex_code.']/u', '', $clear_string);

		return $clear_string;
	}
}